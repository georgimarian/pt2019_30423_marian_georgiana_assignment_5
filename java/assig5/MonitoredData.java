package assig5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class MonitoredData {
	private String startTime;
	private String endTime;
	private String activityLabel;

	public MonitoredData(String startTime, String endTime, String activityLabel) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}

	public String getActivity() {
		return activityLabel;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	/**
	 * Method that subtracts start time from end time
	 * 
	 * @return returns duration of activity in minutes
	 */
	public long getDuration() {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date d1 = formatter.parse(endTime);
			Date d2 = formatter.parse(startTime);
			LocalDateTime ld1 = d1.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			LocalDateTime ld2 = d2.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			Duration diff = Duration.between(ld2, ld1);
			return diff.toMinutes();
		} catch (Exception e) {

		}
		return 0;
	}

	/**
	 * Returns the readable version of an object of this class
	 */
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(startTime + "  " + endTime + "  " + activityLabel);
		return stringBuilder.toString();
	}
}
