package assig5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FileRead {
	private static final String FILENAME = "C:\\Users\\georg\\Desktop\\PT2019\\PT2019_30423_Marian_Georgiana\\pt2019_30423_marian_georgiana_assignment_5\\Activities.txt";

	/**
	 * Reads data from file using streams
	 * @return returns the list of read MonitoredData
	 */
	public List<MonitoredData> readFromFile() {
		List<MonitoredData> readData = new ArrayList<MonitoredData>();
		try (Stream<String> stream = Files.lines(Paths.get(FILENAME))) {

			stream.forEach(a -> readData
					.add(new MonitoredData(a.substring(0, 19), a.substring(21, 40), a.substring(42, a.length()-1))));

		} catch (IOException e) {
			e.printStackTrace();
		}
		return readData;
	}
}
