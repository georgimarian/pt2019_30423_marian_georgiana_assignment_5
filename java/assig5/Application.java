package assig5;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

	/**
	 * Computes the number of days logged in the file
	 * 
	 * @param monitoredData
	 * @return returns number of days
	 */
	private static long numberOfDays(List<MonitoredData> monitoredData) {
		Stream<Object> start = monitoredData.stream().map(k -> k.getStartTime().split(" ")[0]);
		Stream<Object> end = monitoredData.stream().map(k -> k.getStartTime().split(" ")[0]);
		return Stream.concat(start, end).distinct().count();
	}

	/**
	 * Maps Activities to their occurrence number over the entire monitoring period
	 * 
	 * @param monitoredData
	 * @return returns a Map having Activities as keys and their occurrence number
	 *         as value
	 */
	private static Map<String, Long> activityOccurrence(List<MonitoredData> monitoredData) {
		return monitoredData.stream().collect(Collectors.groupingBy(k -> k.getActivity(), Collectors.counting()));
	}

	/**
	 * Maps each activity in the file to its duration
	 * 
	 * @param monitoredData
	 * @return
	 */
	private static Map<String, Long> activityDuration(List<MonitoredData> monitoredData) {
		return monitoredData.stream()
				.collect(Collectors.toMap(k -> k.getStartTime() + " " + k.getActivity(), k -> k.getDuration()));
	}

	/**
	 * Maps each Activity to its total duration expressed in minutes
	 * 
	 * @param monitoredData
	 * @return returns a Map with Activity as key and total duration in minutes as
	 *         value
	 */
	private static Map<String, Long> computeTotalDuration(List<MonitoredData> monitoredData) {
		return monitoredData.stream()
				.collect(Collectors.groupingBy(k -> k.getActivity(), Collectors.summingLong(k -> k.getDuration())));
	}

	/**
	 * Filters activities having 90% of records lasting < 5 minutes
	 * 
	 * @param monitoredData
	 * @return returns the List of activities
	 */
	private static List<String> filterActivities(List<MonitoredData> monitoredData) {
		List<String> result = new ArrayList<String>();
		Map<String, Long> occurrencesLessThan5 = monitoredData.stream().filter(a -> a.getDuration() < 5)
				.collect(Collectors.groupingBy(a -> a.getActivity(), Collectors.counting()));
		Map<String, Long> activityOccurrence = activityOccurrence(monitoredData);
		for (Map.Entry<String, Long> entry : occurrencesLessThan5.entrySet()) {
			if ((float) entry.getValue() > 0.9 * (float) activityOccurrence.get(entry.getKey())) {
				result.add(entry.getKey());
			}
		}
		return result;
	}

	/**
	 * Returns a Map having a Date as key and another Map as value, having Activity
	 * as key and occurrences as value
	 * 
	 * @param monitoredData
	 * @return Map of dates and related activities
	 */
	private static Map<String, Map<String, Long>> activitiesPerDay(List<MonitoredData> monitoredData) {
		return monitoredData.stream().collect(Collectors.groupingBy(k -> k.getStartTime().split(" ")[0],
				Collectors.groupingBy(k -> k.getActivity(), Collectors.counting())));
	}

	public static void main(String[] args) {
		FileRead fileReader = new FileRead();
		List<MonitoredData> monitoredData = fileReader.readFromFile();

		System.out.println("________The occurrence of each activity: ");
		Map<String, Long> m = activityOccurrence(monitoredData);
		for(Map.Entry<String, Long> e : m.entrySet()) {
			System.out.println(e.toString());
		}

		System.out.println("________The total duration of each activity: ");
		Map<String, Long> n = computeTotalDuration(monitoredData);
		for(Entry<String, Long> e : n.entrySet()) {
			System.out.println(e.toString());
		}

		System.out.println("________The duration of each activity: ");
		Map<String, Long> d = activityDuration(monitoredData);
		System.out.println(d.toString());

		System.out.println("________Number of days: ");
		System.out.println(numberOfDays(monitoredData));

		System.out.println("________Activities per day: ");
		for(Entry<String, Map<String, Long>> e : activitiesPerDay(monitoredData).entrySet()) {
			System.out.println(e.toString());
		}
		

		System.out.println("_____Filter activities with 90% <5");
		System.out.println(filterActivities(monitoredData));
	}
}
